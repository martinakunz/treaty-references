# Treaty References

This repository contains:

- A bibliographic database with treaty references (`treatyrefs.bib`)
- A text file listing OSCOLA-compliant treaty references in alphabetical order (`treatyrefs_oscola_alphabetical.txt`)
- A text file listing OSCOLA-compliant treaty references in chronological order by treaty adoption date (`treatyrefs_oscola_chronological.txt`)
- A tabular data file with treaty reference data from which the other files can be generated (`treatyrefs.csv`), with the following columns:
  - `treatyLabel`: bibkey (unique identifier) for each treaty
  - `shortTitleBib`: shortened treaty title for use in subsequent references
  - `treatyTitle`: treaty title as used in citations, in title case
  - `treatyTitleUNTS`: treaty title as retrieved from UNTS, often in sentence case
  - `treatyAdoptionDate`: date of adoption/conclusion of the treaty
  - `treatyEIFdate`: date of entry into force of the treaty (if applicable)
  - `UNTSvolRef`: UNTS volume and page number retrieved from UNTS online (where available)
  - `UNTStreatyRecordURL`: link to UNTS treaty page for each treaty (where available)
  - `UNTStrLastRetrieved`: date of last retrieval of UNTS treaty record
  - `ATSref`: ATS reference for treaties published in ATS
  - `OrgRef`: UN or IMO Doc reference (where necessary)
  - `ILMref`: ILM publication reference (where necessary)
- `treatyrefbib.py`: a Python script to generate `treatyrefs.bib` from data in `treatyrefs.csv`
- `treatyreflist.py`: a Python script to generate `treatyrefs_oscola_alphabetical.txt` and `treatyrefs_oscola_chronological.txt` from data in `treatyrefs.csv`

***Note:*** _The files are largely self-explanatory, feel free to just download them and get on with your day without reading any of the following._

The `.bib` file is designed to be used with the excellent [`oscola`](https://www.ctan.org/pkg/oscola) Biblatex package by Paul Stanley or one of the [`oxref`](https://www.ctan.org/pkg/biblatex-oxref) packages which use Stanley's conventions for legal references but otherwise offer a wider range of styles, such as numbered endnotes (with `oxnum`). Any proper reference management software should be able to import and export `.bib` files, but I doubt the treaty-specific features can be used in proprietary software which by nature restricts users' [freedom](https://www.gnu.org/philosophy/free-sw.html) to adapt it for their purposes. Some of the advantages of `oscola` (which is part of any standard TeXLive and MiKTeX distribution) are automatic citation number tracking ("(n 7)" style), bibliographies separated into customizable types (legislation, treaties, case law etc.), an automatic index of treaties with page numbers on which they are cited, and of course the convenience of a simple `\cite{NPT}` LaTeX command (or `cite:NPT` with org-ref in Emacs) to produce an OSCOLA-compliant citation of the Nuclear Non-Proliferation Treaty.

As the number of people citing treaties _and_ using LaTeX is probably small compared to that of people needing correct treaty references but using other, or no reference management software, the two `.txt` files provide lists for easy copy-pasting into any text editor or word processing software. If there is demand, I could add similar text files for other citation styles or languages (please raise an [Issue](https://gitlab.com/martinakunz/treaty-references/-/issues) if interested). The `treatyreflist.py` script can be used to generate additional treaty lists and there are some code samples below to show how. Regarding file formats, `.txt` is the most portable format for text files, and `.csv` is the equivalent for tabular file formats. The latter can easily be opened in LibreOffice Calc, Microsoft Excel or similar spreadsheet software.

The bibliographic database currently includes over 100 multilateral treaties and most of the data was retrieved automatically from the online database of the [United Nations Treaty Series](https://treaties.un.org/Pages/Content.aspx?path=DB/UNTS/pageIntro_en.xml) (UNTS, and 'UNTSonline' to refer to the database) with a custom scraper and parser I developed for my PhD research (to be published in due course). Thankfully at some point in the last few years, UNTSonline started including not just the UNTS volume number but also the page number, which is necessary for a complete treaty reference.

A small number of treaties in the files shared do not have an UNTS reference, because they haven't been published in an UNTS volume (yet). In these cases reference is made to the Australian Treaty Series (ATS), an official UN document, or the International Legal Materials (ILM) journal, in this order of preference, based on [OSCOLA 2006 guidance for international law](https://www.law.ox.ac.uk/sites/files/oxlaw/oscola_2006_citing_international_law.pdf) (OSCOLA 2006 IL) (p.25):

> Where applicable, cite the treaty series in the following order of preference:
> - primary international treaty series, eg UNTS (United Nations Treaty Series), CTS (Consolidated Treaty Series) or LNTS (League of Nations Treaty Series);
> - official treaty series of one of the States parties, eg UKTS (UK Treaty Series), (ATS) (Australian Treaty Series); and
> - other international treaty series (eg British and Foreign State Papers).
>
> (...) For post-1960 treaties not yet published in an official series, the usual source is International Legal Materials (ILM).

Strictly speaking, other governmental treaty series should have been consulted before resorting to ILM references, and I did search the UK Treaty Series for some of them, but often the same reason for absence from UNTS also applies to other official treaty series (e.g. the treaty has not entered into force, or only recently came into effect). The spreadsheet includes ATS references for all treaties which have been published in this series, not just those without an UNTS reference, and were also automatically retrieved. IO and ILM references were entered manually.

A quick comment on the benefits of automation and sharing of treaty reference data. Anyone who has ever had to find or double-check treaty citations on a tight deadline will probably agree that it is a time-consuming, error-prone and rather tedious task. Unsurprisingly, erroneous and outdated citations abound. Automated retrieval and sharing of data in an open format that suits the workflow of users can thus save time and potentially improve publication quality. Moreover, when the citation style mandates mention of the entry into force date (as it should), regular automated updating saves even more time/hassle, especially for commentators who frequently write about recent treaties which could enter into force in-between publications. Even the OSCOLA 2006 IL authors did not make sure their references are up-to-date (see e.g. the treaty citation in the [ILM section](#ilm-references) below, which omits the entry into force date of a treaty that came into effect in 2002), probably copying from a prior version. Some tips for downloading and merging updates with your own treaty reference files can be found at the [end](#customization).

More treaties will be added in the near future, but there is no clear target size for the database yet. As far as I know, there are over 70'000 treaties in UNTS, of which over 6000 are multilateral agreements. In principle, all treaties could be included (if permitted by the copyright holder), the file types used are very space-efficient. The bottleneck is manual verification of the output, of e.g. the treaty title cleaning functions. But actual need is also an open question. My guess is that only a couple hundred treaties are regularly cited in documents that require proper citation (official documents and reports, legal scholarship, student papers, submissions to legal proceedings etc.). Hence, for now my plan is to add all the treaties I cite in my publications, or maybe only those that I cite more than once, and then expand the project based on user requests (if any).

For the detail-oriented, the curious, and/or budding programmers, data processing and possible adaptations are explained under the following headings:

- [Treaty titles and shorthands](#treaty-titles-and-shorthands)
- [Treaty dates](#treaty-dates)
- [UNTS references](#unts-references)
- [ATS references](#ats-references)
- [IO document references](#io-document-references)
- [ILM references](#ilm-references)
- [Customization](#customization)

Any feedback, data and code edits are most welcome. All code on this page is shared under [GNU AGPLv3](https://www.gnu.org/licenses/agpl-3.0.html) and text under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/). Key software packages and versions used (on a GNU/Linux Debian bullseye distribution, but other operating systems and package versions should work as well, with minor modifications) are:

- GNU Emacs 27.1 with orgmode 9.4 (great for writing, project management and coding in many [languages](https://orgmode.org/worg/org-contrib/babel/languages/index.html))
- Python 3.8.6 with numpy 1.19.4, pandas 1.1.5, titlecase 1.1.1
- Texlive 2020.20201203 with oscola 1.7

<!-- TODO mention problems with Windows, e.g. file path specs, or link to guide/comparison/tutorial -->

A good introduction to natural language processing with Python is the book available at http://www.nltk.org/book/ (especially the preface and first chapter are great for learning the basics, even if the NLTK package is not needed for simple operations like the ones discussed below).

## Treaty titles and shorthands

Treaty titles are surprisingly inconsistent across databases, which makes merging different datasets more difficult than it should be. Matching only on the first or last few words of the title is no solution, because there are very common variations at the beginning (e.g. place or year of adoption) _and_ at the end of the title (adoption year or mention of annexes). As there is no single/right answer to some of these formatting questions, in this section I will describe the rationale and specifics of my treaty title modification algorithm, with commented code. I strongly believe everyone should learn how to code, and learning by example proved to be the most useful approach for me, so hopefully this will have some educational value to someone.

### Punctuation, whitespace and special characters

First, some basic [string](https://docs.python.org/3/tutorial/introduction.html#strings) cleaning is in order, i.e. removing redundant whitespace, periods and asterisks from the beginning and end of treaty titles, as well as replacing any double whitespaces with a single whitespace anywhere in the string.

As this is the first code example, we also need to import the relevant Python libraries and read the data table into memory. In particular the [`pandas`](https://pandas.pydata.org/) library is made heavy use of, as it defines convenient data manipulation functions. The code blocks are intended to be run in an interactive session, e.g. from within Emacs, for experimentation purposes, and thus differ slightly from the full `.py` scripts in the repository, but the results are the same. Also, anything prepended by a '#' is a comment and skipped by the Python interpreter.

```python
import os
import numpy as np
import pandas as pd

# reading data into a pandas dataframe
treatyrefs = pd.read_csv(os.path.expanduser('~/git/treaty-references/treatyrefs.csv'), encoding='utf-8')
# create a backup of the treatyTitle on file (to compare edits)
treatyrefs['treatyTitle0'] = treatyrefs.treatyTitle.copy()
# copy the title retrieved from UNTS and fill empty values with backup
treatyrefs.treatyTitle = treatyrefs.treatyTitleUNTS.copy().fillna(treatyrefs.treatyTitle0)
# strip chars from left and right end of string and remove unnecessary whitespace anywhere
treatyrefs.treatyTitle = treatyrefs.treatyTitle.str.strip(' .*').str.replace('  ',' ')

```

For the sake of simplicity this presentation will be limited to processing of treaty titles found in UNTS, the remaining ones being copied over from the backup (with the [`.fillna()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Series.fillna.html) function).

There's an additional minor punctuation edit that consistency-focused (not to say -obsessed) people like me might want to make, and that is in the titles of the Additional Protocols I and II, which in UNTS are entered as:

> - Protocol additional to the Geneva Conventions of 12 August 1949, and relating to the protection of victims of international armed conflicts (Protocol I)
> - Protocol Additional to the Geneva Conventions of 12 August 1949 and relating to the protection of victims of non-international armed conflicts (Protocol II)

Either option seems fine, I opted for adding a comma to the second one:

```python
treatyrefs.treatyTitle = treatyrefs.treatyTitle.str.replace('1949 and relating', '1949, and relating')
```

Capitalization is also inconsistent across the two titles, but will be addressed [below](#proper-title-case).

### Information in parentheses

In some cases parentheses are clearly part of the treaty title, containing relevant information, whereas in others they are superfluous. One type of unnecessary information in parentheses is about annexes or translations included in the database, e.g. "(with annexed General Regulations and Financial Regulations)". This kind of information may make sense in the context of the database, but not for treaty references.

After looking at the list of all treaty titles containing parentheses, a simple rule turned out to be sufficient: if there are parentheses at the end of the title string, remove them and anything they contain unless the first word after the opening parenthesis is 'Protocol'. This keeps the parentheses in the above-mentioned Additional Protocols, in ILO conventions which usually start with "Convention (No. ...) concerning", and the Biological Weapons Convention ("bacteriological (biological) and toxin weapons"), while removing them elsewhere. This is again a matter of one line of code, thanks to the wonderful invention of regular expressions:

```python
treatyrefs.treatyTitle = treatyrefs.treatyTitle.str.replace(' ?[(](?!Protocol)[^(]+[)]$','')
```
If you're wondering, [`.str.replace()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Series.str.replace.html) is based on Python's [`re`](https://docs.python.org/3/library/re.html) module, to which there is a good introductory [HOWTO](https://docs.python.org/3/howto/regex.html) in the documentation.

### Treaty adoption year and duplicate titles

An additional question that presents itself is whether to keep treaty adoption years added to some treaty titles, most commonly of IMO conventions and international commodity agreements. Especially when multiple successive agreements with the same title are cited in the same publication, it stands to reason that the year should be added to the title (or at least the short titles should distinguish between them by adding the year or place of adoption). However, for treaties which do not yet, and may never, have subsequent versions, it seems redundant, because full OSCOLA-style treaty citations already contains the date of adoption. Some examples of treaties with redundant adoption years in UNTS titles are:

> - International Convention on Salvage, 1989
> - International Convention on oil pollution preparedness, response and cooperation, 1990
> - International Convention on Arrest of Ships, 1999

Whether or not there are other agreements with the same title cannot be inferred from such a small dataset, but this factor was already taken into account when (manually) entering treaty labels, used as identifiers in the `.bib` database. Whenever there is another agreement with the same title, a year was added to the label, e.g. MARPOL73 or RoadTrafficConv68. This can be used for treaty title editing, removing or adding years accordingly. Years are added in the ', YYYY' notation as in the examples cited, except when the agreement title starts with the term 'Protocol', in which case the year is prepended, as in "1996 Protocol to the Convention on...". This is done in three steps, using regular expressions and filtering conditions, but first a helper column is added to the dataframe with the adoption year extracted from the date (which is in ISO format, i.e. YYYY-mm-dd).

```python
# extract year from adoption date and add as a new column to the table
treatyrefs['treatyAdoptionYear'] = treatyrefs.treatyAdoptionDate.str[:4]
# remove years from the end of titles of treaties which do not have a digit in their treatyLabel
treatyrefs.loc[~treatyrefs.treatyLabel.str.contains('\d') & treatyrefs.treatyTitle.str.contains(', \d{4}$'), 'treatyTitle'] = treatyrefs.treatyTitle.str.replace(', \d{4}$','')
# append the year to titles which do not have any and which do not start with 'Protocol' and whose treatyLabel contains a digit
treatyrefs.loc[treatyrefs.treatyLabel.str.contains('\d') & ~treatyrefs.treatyTitle.str.contains('\d{4}') & ~treatyrefs.treatyTitle.str.startswith('Protocol'), 'treatyTitle'] = treatyrefs.treatyTitle + ', ' + treatyrefs.treatyAdoptionYear
# prepend the year to titles which do not have a year at the end of the title string and which start with 'Protocol' and whose treatyLabel contains a digit
treatyrefs.loc[treatyrefs.treatyLabel.str.contains('\d') & ~treatyrefs.treatyTitle.str.contains(', \d{4}$') & treatyrefs.treatyTitle.str.startswith('Protocol'), 'treatyTitle'] = treatyrefs.treatyAdoptionYear + ' ' + treatyrefs.treatyTitle
```

***Note:*** _`\d{4}` means four digits and `$` refers to the end of the string (in a regex pattern). `~` means NOT(...) in Python._

<!-- TODO consider refactoring code, maybe a for loop would be easier to read/understand -->

### Proper title case

Title case is used in OSCOLA and many other treaty citation styles, but unfortunately not consistently in UNTS and other treaty databases. Moreover, out-of-the box conversion tools, such as Python's in-built `.title()` or even the more sophisticated `titlecase()` function of the [`python-titlecase`](https://github.com/ppannuto/python-titlecase) library are not sufficient to get the expected results. It may be that capitalization of treaty titles follows slightly different conventions than that of, e.g. newspapers (`python-titlecase` is based on the New York Times Manual of Style). For instance, 'from', 'with', 'their', 'that', 'those' and 'into' are hardly ever capitalized in treaty titles (I double-checked), but are all converted to title case by the `titlecase()` function. Fortunately, it is easy to add exceptions with a callback function that is called for each word processed.

```python
from titlecase import titlecase

# define words that should be lower case in treaty titles
def titlelow(word, **kwargs):
  if word.lower() in 'from with their that those into regard concerning relating including against':
    return word.lower()
    
# apply the customized function to each treaty title
treatyrefs.treatyTitle = treatyrefs.treatyTitle.apply(lambda x: titlecase(x, callback=titlelow))
```

'Particularly' and 'especially' are also candidates for adding to the `titlelow` exceptions, but the evidence I have seen so far was inconclusive, both versions being used (e.g. in UNCCD and the Ramsar Convention).

After running the preceding (or modified) code on your own computer, it is always a good idea to compare the results with the backup we did in the beginning:

```python
print(treatyrefs.treatyTitle.equals(treatyrefs.treatyTitle0))
```
This should return `True`. If it doesn't, then you could check the diverging, newly edited titles with:

```python
print(treatyrefs.treatyTitle[treatyrefs.treatyTitle != treatyrefs.treatyTitle0].values)
```
This returns the subset of treaty titles which differ even in the slightest from the corresponding value in the `treatyTitle0` column.

### Short titles and labels

Two additional columns in the spreadsheet are worth mentioning: `treatyLabel` and `shortTitleBib`. They are not included in the treaty references listed in the `.txt` files, because shorthands and acronyms are typically only used if treaties are cited more than once in a publication, and the form they take depends on the target audience, other treaties cited, and whether the reference needs to pinpoint to a particular version of the treaty (e.g. if treaty evolution over time is discussed). For instance, in a narrowly focused publication the shorthand "Vienna Convention" may be perfectly unambiguous, even without a citation tracking number, but this typically has to be decided ad hoc. As my doctoral research covers all the treaties in the database (and more), I opted for a short title that is maximally informative to a generalist while still being as concise as possible. The same rationale applies to `treatyLabel`, which, as mentioned above, is used as a bibliographic key in the `.bib` file and for certain research outputs like data visualizations. If you choose to use the `oscola` package, I highly recommend reading about the differences between `shorttitle`, `shorthand` (and maybe `indextitle`) in the package documentation.

These should not be seen as permanent identifiers, I may change them at any time depending on the evolving needs of database users, including myself. One potential extension would be to add a column 'treatyAcronym' to the spreadsheet, which contains the most commonly used acronym for treaties that have one, so that it can be entered into the `shorthand` field during `.bib` file generation, or to the end of treaty citations in `.txt` files. At present some short titles and treaty labels contain acronyms, but not all, depending on how widely used the acronym is and on how easy it is to shorten the title (or how short it is to begin with). For example, "World Heritage Convention" is the `shortTitleBib` for the UNESCO Convention for the Protection of the World Cultural and Natural Heritage, the `treatyLabel` being 'WHC', but some authors may prefer to use the acronym as a shorthand, especially if they cite a lot of treaty provisions. Copying acronyms from the `treatyLabel` column to a new column would not take more than one line of code, and some acronyms could be generated from the treaty label or short title (concatenating upper case letters), but that would not work for all treaties and thus would require some manual data entry/modification. The subsection on [text files](#using-text-files) has some advice and sample code for adding a manually entered treatyAcronym column to a newly downloaded spreadsheet in the context of an update, and producing text files with the acronym appended to treaty citations.


## Treaty dates

OSCOLA 2006 IL (p.25) on treaty dates:

> If parties can accede to the treaty (which will be the case for most multilateral treaties), cite the full date upon which the treaty was opened for signature. Otherwise, cite the date that it was signed or adopted. If available, then give the date it entered into force. If there is both a date of adoption and a date on which the treaty opened for signature, cite the dates in that order.

This emphasis on the opening for signature as more important than adoption is not in line with the practice of global organizations and treaty databases. Whenever the two events are separate in time, only adoption is included in databases like UNTS or ATS. The focus on opening for signatures in OSCOLA 2006 IL may be due to the authors being more familiar with treaties from regional organizations like the Council of Europe (CoE) which seem to have adoption and opening on the same day and report the date as "Opening of the treaty" in their [database](https://www.coe.int/en/web/conventions/full-list). Be that as it may, it makes most sense to report the date as it is conceived of by the states or organizations that adopt the agreement. If conclusion/adoption is how it is framed, and it happens to coincide with the opening for signature (because signing often serves as authentication of the text), it should arguably still be cited as adoption, OSCOLA nothwithstanding.

At present this database contains mostly open multilateral treaties drawn from UNTS and ATS, and thus adoption and entry into force are the dates included. As and when I add treaties from regional organizations which report opening for signature rather than adoption, I will adapt the files accordingly.

Any and all of the relevant treaty dates can be entered into the `.bib` database in the `execution` field which is special to treaties, and is formatted as follows: `execution = {adopted=1980-10-10 and inforce=1983-12-02}`. The keywords are transformed into the expected text in the output (same for the dates):

| Keyword | Text                 |
|---------|----------------------|
| signed  | signed               |
| adopted | adopted              |
| opened  | opened for signature |
| inforce | entered into force   |

If `adopted`, `opened` and `inforce` are all in the database, they will all be included in the citation. As extracting opening/signature dates from treaty texts would require quite a bit of effort without much added benefit, especially as word limits for publications are often tight anyway, I will not aim to eventually have three dates in the database for all treaties that have them.  


## UNTS references

UNTS references are relatively straightforward in OSCOLA: volume + 'UNTS' + page number.

The `.bib` file includes the abbreviation with periods (U.N.T.S.) because other citation styles require periods and it is always easier to remove them automatically than to add them.

The page number is a bit less obvious. Unlike other citation styles, OSCOLA 2006 IL (p.25) stipulates that:

> Treaties should be cited from the Final Act (if that appears before the text of the treaty itself). An example is the Convention relating to the Status of Refugees: the Final Act appears at 189 UNTS 137, while the text of the treaty itself begins at 189 UNTS 150. The correct citation for the treaty is 189 UNTS 137. (It is not necessary to include the words ‘Final Act’ in the citation of the treaty title.)

So, someone only interested in the treaty text itself would need to flip/scroll through thirteen pages before getting there, and this would still be nothing compared to other Final Acts. Moreover, as this project uses automated harvesting of information, the page number will be whatever the UNTS treaty section enters into their database. In this example, the [UNTS page](https://treaties.un.org/Pages/showDetails.aspx?objid=080000028003002e&clang=_en) for the treaty in question reports the UNTS volume reference as "189 (p.137)", but in other cases it may not be entirely OSCOLA-compliant.

In the spreadsheet, `UNTSvolRef` shows the exact string copied from the online treaty page by automated means. In the Python scripts, the relevant information is extracted in this way:

```python
# extract volume number (the first number appearing in the string)
treatyrefs['UNTSvol'] = treatyrefs.UNTSvolRef.str.extract('(\d+)', expand=False)
# extract the page number
treatyrefs['UNTSpage'] = treatyrefs.UNTSvolRef.str.extract('\d+ .p.(\d+)', expand=False)
# update wrongly extracted page nb
treatyrefs.loc[treatyrefs.treatyLabel=='SOLAS74', 'UNTSpage'] = '278'
```

The final line of code is due to the fact that the "UNTS Volume Number" field on the relevant UNTSonline [page](https://treaties.un.org/Pages/showDetails.aspx?objid=08000002800ec37f&clang=_en) currently shows "1184, 1185 (p.2)", and the number 2 gets extracted as page number, when the correct reference is actually 1184 UNTS 278. This ad hoc correction will be removed once the entry is updated in UNTSonline.

The `.csv` file also includes the link to each UNTS treaty page in the column `UNTStreatyRecordURL` (with its date of last retrieval under `UNTStrLastRetrieved`) for verification, reproducibility, source acknowledgement, and general informational and educational purposes.


## ATS references

The sample ATS reference given in OSCOLA 2006 IL (p.26) is "ATS 1994 15", i.e. the year _after_ the acronym, unlike most other publication types in OSCOLA. This is the way the reference is reproduced in the two `.txt` files and the `.csv` file.

The `oscola` Biblatex package does not handle this out of the box. If the year is included in the `year` or `date` field, it gets placed before 'ATS' in square brackets or omitted if there is also a `volume` field. As for the second part of the reference, the `number` field would be most appropriate, but any information in this field (or the equivalent `issue` field) is ignored for treaty entries. There are multiple ways in which the information can be entered in the `.bib` file such that the output conforms to OSCOLA 2006 IL, but suffice to say that this is the one chosen here:
```
  reporter     = {A.T.S.},
  series       = {1994},
  pages        = {15},
```
Obtaining the commonly used "[1994] ATS 15" citation style would be as easy as replacing all instances of `series` with `year`.


## IO document references

While this is not explicitly mentioned in OSCOLA 2006 IL (except for GATT/WTO agreements on p.27), for some treaties that have not (yet) entered into force an UN Doc or other official IO doc reference seems most appropriate, because UNTS and ATS only cover treaties in force, and ILM only starts in the 1960s, is incomplete, non-official, and paywalled.

As there are only a handful of treaties in the `.bib` file falling into this category, I added these references manually to the spreadsheet, in the column `OrgRef`. Several of these may get an UNTS reference in one of the future updates of the database, once they have been published in an UNTS volume, in which case the UN or IMO Doc reference will be dropped.

`oscola` has an `undoc` entry subtype, but that did not seem adequate for treaty references. Thus, the normal `piltreaty` entry subtype was used like for other treaties, simply putting the whole document reference into the `series` field like so:

```bibtex
@legal{WreckRemovalConv,
  entrysubtype = {piltreaty},
  title        = {Nairobi International Convention on the Removal of Wrecks},
  execution    = {adopted=2007-05-18 and inforce=2015-04-14},
  pagination   = {article},
  series       = {IMO Doc LEG/CONF. 16/19},
  note         = {},
  shorttitle   = {Wreck Removal Convention},
}
```
Which results in:
> Nairobi International Convention on the Removal of Wrecks (adopted 18 May 2007, entered into force 14 April 2015) IMO Doc LEG/CONF 16/19.

"IMO Doc LEG/CONF.16/19" would be preferrable, but periods are removed automatically and thus this string would become "IMO Doc LEG/CONF16/19".

<!-- TODO edit files such that .txt files get the preferred version -->

## ILM references

OSCOLA 2006 IL has one example of a treaty with an ILM reference (on p. 26):

> UNGA International Convention for the Suppression of the Financing of Terrorism (adopted 9 December 1999, opened for signature 10 January 2000) (2000) 39 ILM 270

Apart from the questionable inclusion of the UNGA as author (and the missing entry into force date), adding the year of the ILM publication is not a particularly common practice for treaty references either, given that treaties already have their own dates to cite. However, as some publishers require it and as removing information is always easier than adding it ad hoc, the `ILMref` column in the spreadsheet and the ILM references in the `.txt` files include the year of ILM publications. If you would like to generate the two text files without ILM year, you could simply add the following line of code to `treatyreflist.py` (e.g. on line 8, just after loading the data):

```python
treatyrefs.ILMref = treatyrefs.ILMref.str.replace('\(\d+{4}\) ','')
```
Running the Python script produces the text files without updating the spreadsheet, so you would still have the year information available somewhere. Note that the program overwrites the text files without warning, so rename them first if need be.

As for the `.bib` file, this is how ILM treaty publication information is represented:

```
  journaltitle = {I.L.M.},
  year         = {2000},
  volume       = {39},
  pages        = {270},
```
Because the `year` field is ignored for treaties that have an `execution` field, this renders as "39 ILM 270".

If `options = {year-essential=true}` is added to the entry, then the reference becomes "[2000] 39 ILM 270", i.e. square brackets rather than parentheses as in the OSCOLA example. As I prefer omitting ILM publication years for treaty references, I am not bothered by this deviation from the style guide, it is just something to bear in mind. A workaround would be to include the year in the volume info, e.g. `volume = {(2000) 39}`.


## Customization

I know from experience that in the beginning even simple digital automation tasks can seem daunting, so much so that one might fall back on manual processing despite its drawbacks. This section therefore outlines a few possible workflows for various skill/experience/courage levels.

### Using git

This being a Git repository, the obvious choice for regular data updates is [Git](https://git-scm.com/) itself. An initial `git clone`, then regularly `git pull`, problem solved (merging automatically). In Emacs, `magit` is a convenient wrapper.

However, I doubt this little project/use-case alone would provide sufficient motivation to get started with Git. So, on to other options.

### Using Zotero, Endnote or Mendeley

Download and import the `.bib` file into your reference manager of choice (e.g. [Zotero guide](https://www.zotero.org/support/kb/importing_standardized_formats)), then use it to look up treaty references when needed. If there is a suitable treaty citation tool/plugin but the fields used are different from the ones in `treatyrefs.bib`, you could edit the Python script `treatyrefbib.py` accordingly and then use it to generate a customized `.bib` file to import into your bibliographic database. I intentionally use a very simple string concatenation method rather than a full-blown Python bibtex library to lower the barrier for user-defined tweaking. Basically, this code block generates UNTS references in biblatex format:

```python
entries = ''
# UNTS
for i in treatyrefs[treatyrefs.UNTSpage.notna()].index:
  entry = '@legal{'+treatyrefs.treatyLabel[i]+''',
  entrysubtype = {piltreaty},
  title        = {'''+treatyrefs.treatyTitle[i]+'''},
  execution    = {adopted='''+treatyrefs.treatyAdoptionDate[i]+' and inforce='+treatyrefs.treatyEIFdate[i]+'''},
  pagination   = {article},
  reporter     = {U.N.T.S.},
  volume       = {'''+treatyrefs.UNTSvol[i]+'''},
  pages        = {'''+treatyrefs.UNTSpage[i]+'''},
  note         = {},
  shorttitle   = {'''+treatyrefs.shortTitleBib[i]+'''},
}

'''
  entries += entry
```

For each row in the treatyrefs table that has an UNTS page number (extracted in a previous step), a properly formatted entry is produced, then appended to a string (`entries`) which is saved to file at the end. If you enter your own preferred treaty label or short title in the spreadsheet, then you would just need to make sure that the Python script loads your modified version by specifying the right filename in `treatyrefs = pd.read_csv('treatyrefs.csv', encoding='utf-8')`.

I am not sure whether data updating can be automated in these reference managers, but it might be possible to delete all entries of a certain type and then import a new `.bib` file whenever there is a relevant update.

### Using text files

If all you want is a text file with treaty citations to copy-paste into Microsoft Word and you make your edits ad hoc, then all you need to do is occasionally download a new version of your preferred `.txt` file. You may wish to register with Gitlab to get notified of new releases by email.

Should you wish to make permanent edits to your treaty reference list (e.g. adding treaty shorthands in parentheses or removing ILM publication years), while also growing and updating your database periodically, then there are various options:

- Edit `treatyreflist.py` and occasionally download `treatyrefs.csv`, generating your own text files
  - Fetching the `.csv` file and running the Python script could be scheduled with cron job or similar tool
  - Is ideal if you want to make many edits of the same type (e.g. changing date format, removing entry into force date, modifying punctuation etc.)
  - Also works well for specific one-off corrections (see examples of SOLAS74 and Additional Protocols I & II above)
  - Keeps a convenient record of your edits (good for reproducibility and review)
  - Make sure to occasionally check the corresponding `.py` script in this repo for updates/corrections
- Edit `treatyrefs.csv` and save under a new name, then edit `treatyreflist.py` to merge your own spreadsheet with a newly downloaded one (once available)
  - Can also be scheduled to run automatically once set up
  - Might be more convenient for making many non-routine modifications (e.g. defining a handpicked shorthand/acronym for each treaty)

To give a specific example, let's say you manually create a column called `treatyAcronym` and you define an acronym that you would like to add in parentheses at the end of a treaty citation during the text file generation process. A few months later the database in this repository has grown and you would like to integrate the update with your own edits. Pandas' [`.merge()`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.merge.html) function will handle this in a breeze. Here is how you could edit your `treatyreflist.py`:

```python
# loading data from the newly downloaded file
treatyrefs = pd.read_csv('treatyrefs.csv',
                         encoding='utf-8',
                         parse_dates=['treatyAdoptionDate', 'treatyEIFdate'],
                         infer_datetime_format=True)

# loading data from the locally edited file (this has an extra column called treatyAcronym)
mytreatyrefs = pd.read_csv('mytreatyrefs.csv',
                         encoding='utf-8',
                         parse_dates=['treatyAdoptionDate', 'treatyEIFdate'],
                         infer_datetime_format=True)

# adding treatyAcronym to treatyrefs with UNTStreatyRecordURL as merge key
treatyrefs = treatyrefs.merge(mytreatyrefs[['UNTStreatyRecordURL','treatyAcronym']],
                        how='left',
			on='UNTStreatyRecordURL')
# ...
# combining the two parts of the reference
treatyrefs['oscolaRef'] = treatyrefs.refpart1 + treatyrefs.refpart2
# add acronym in parentheses to oscolaRef where available
treatyrefs.loc[treatyrefs.treatyAcronym.notna(), 'oscolaRef'] = treatyrefs.oscolaRef + ' (' + treatyrefs.treatyAcronym + ')'
# optionally save the merged and updated dataframe to file
treatyrefs.to_csv('treatyrefs_merged.csv', encoding='utf-8', index=False)
```

Putting the acronym in single or double quotation marks is fine too, so long as it doesn't interfere with Python's string concatenation (the [string](https://docs.python.org/3/tutorial/introduction.html#strings) tutorial might come in handy again, and/or some trial and error in the interactive interpreter).

Having saved the merged dataframe, you could then go on to define acronyms for the newly added treaties and save the manually edited file again to `mytreatyrefs.csv`, ready for the next iteration of updates. In this scenario, as in the previous one, you would not manually edit the `.txt` files, but treat them purely as output, to be overwritten each time.

Another issue that is important to bear in mind is that for merging you always need one or more keys to merge on. In the code example I used the column `UNTStreatyRecordURL` because as far as I know the link never changes, but as mentioned in the introduction, not all treaties have an UNTS page (yet). `ATSref` is also unlikely to change and is available for over 80 treaties. Eventually `treatyLabel` could serve this purpose, but I cannot currently guarantee that it will stay the same for each treaty.

Hopefully these examples and explanations have been useful and have given you ideas for how to use this or similar projects in your own work or studies. 

