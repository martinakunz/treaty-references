import numpy as np
import pandas as pd

treatyrefs = pd.read_csv('treatyrefs.csv',
                         encoding='utf-8',
                         parse_dates=['treatyAdoptionDate', 'treatyEIFdate'],
                         infer_datetime_format=True)

treatyrefs['UNTSvol'] = treatyrefs.UNTSvolRef.str.extract('(\d+)', expand=False)
treatyrefs['UNTSpage'] = treatyrefs.UNTSvolRef.str.extract('\d+ .p.(\d+)', expand=False)
# update wrongly extracted page nb (TODO remove once fixed in UNTS)
treatyrefs.loc[treatyrefs.treatyLabel=='SOLAS74', 'UNTSpage'] = '278'

# reformat dates
adoption = treatyrefs.treatyAdoptionDate.dt.strftime('%-d %B %Y')
eif = treatyrefs.treatyEIFdate.dt.strftime('%-d %B %Y').fillna('NaT')

# construct reference incrementally
treatyrefs['refpart1'] = treatyrefs.treatyTitle + ' (adopted ' + adoption + ', entered into force ' + eif + ') '
treatyrefs.refpart1 = treatyrefs.refpart1.str.replace(', entered into force NaT','')
treatyrefs.loc[treatyrefs.UNTSpage.notna(), 'refpart2'] = treatyrefs.UNTSvol + ' UNTS ' + treatyrefs.UNTSpage
treatyrefs.refpart2 = treatyrefs.refpart2.fillna(treatyrefs.ATSref).fillna(treatyrefs.OrgRef).fillna(treatyrefs.ILMref)
treatyrefs['oscolaRef'] = treatyrefs.refpart1 + treatyrefs.refpart2

# alphabetical list
treatyrefs = treatyrefs.sort_values(by='treatyTitle')
allrefs = '.\n'.join(treatyrefs.oscolaRef.values) + '.\n'
with open('treatyrefs_oscola_alphabetical.txt', 'w', encoding='utf-8') as f:
  f.write(allrefs)

# chronological list
treatyrefs = treatyrefs.sort_values(by=['treatyAdoptionDate','treatyEIFdate'])
allrefs = '.\n'.join(treatyrefs.oscolaRef.values) + '.\n'
with open('treatyrefs_oscola_chronological.txt', 'w', encoding='utf-8') as f:
  f.write(allrefs)
